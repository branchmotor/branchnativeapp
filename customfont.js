import React from 'react'
import { Text, StyleSheet, View, ActivityIndicator } from 'react-native'
import * as Font from 'expo-font'

export default class customFont extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            loading: true,
        }
    }

    async componentWillMount() {
        await Font.loadAsync({
            'ibm-light': require('./assets/fonts/ibm-light.ttf')
        }),
        await Font.loadAsync({
            'ibm-bold': require('./assets/fonts/ibm-bold.ttf')
        })
        this.setState({ loading: false })
    }

    render() {
        if (this.state.loading) {
            return <ActivityIndicator/>
        }
            return (
                <View>
                    <Text style={[styles.defaultStyle, this.props.style]}>
                        {this.props.children}
                    </Text>
                </View>
            )
    }
}

const styles = StyleSheet.create({
    defaultStyle: {
        fontFamily: 'ibm-light'
    },
})