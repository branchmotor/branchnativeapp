import firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyBpPrdTl4df4E1eeGX9mcstM0MEWKjNM0o",
  authDomain: "branch-263701.firebaseapp.com",
  databaseURL: "https://branch-263701.firebaseio.com",
  projectId: "branch-263701",
  storageBucket: "branch-263701.appspot.com",
  messagingSenderId: "74838816843",
  appId: "1:74838816843:web:5ca46e82f1587bc252b5ce",
  measurementId: "G-4CLC0CRTG4",
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);

// <!-- The core Firebase JS SDK is always required and must be listed first -->
// <script src="https://www.gstatic.com/firebasejs/7.13.2/firebase-app.js"></script>

// <!-- TODO: Add SDKs for Firebase products that you want to use
//      https://firebase.google.com/docs/web/setup#available-libraries -->
// <script src="https://www.gstatic.com/firebasejs/7.13.2/firebase-analytics.js"></script>

// <script>
//   // Your web app's Firebase configuration
//
//   // Initialize Firebase
//   firebase.initializeApp(firebaseConfig);
//   firebase.analytics();
// </script>
