import { createStackNavigator } from "react-navigation-stack";
import TalleresScreen from "../screens/Talleres/Talleres";
import ChatScreen from "../screens/Talleres/Chat";
import styles from "../../styles/App.scss";

const TalleresStacks = createStackNavigator(
  {
    Talleres: {
      screen: TalleresScreen,
      navigationOptions: () => ({
        title: "Talleres",
      }),
    },
    chat: {
      screen: ChatScreen,
      navigationOptions: () => ({
        title: "Chat",
      }),
    },
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: "#041c24",
      },
      headerTintColor: "#0396c8",
      headerTitleAlign: "center",
      cardStyle: {
        backgroundColor: "#d7f8f8",
      },
      headerTitleStyle: {
        fontFamily: "IBMPlexSans-Bold",
      },
    },
  }
);

export default TalleresStacks;
