import { createStackNavigator } from "react-navigation-stack";
import VehiculosScreen from "../screens/Vehiculo/Vehiculo";
import EditarVehiculoScreen from "../screens/Vehiculo/EditarVehiculo";
import AgregarVehiculoScreen from "../screens/Vehiculo/AgregarVehiculo";
import DocumentosScreen from "../screens/Vehiculo/Documentos";
import ServiciosScreen from "../screens/Vehiculo/Servicios";
import AgregarServicioScreen from "../screens/Vehiculo/AgregarServicio";

const MotosScreenStacks = createStackNavigator(
  {
    Vehiculo: {
      screen: VehiculosScreen,
      navigationOptions: () => ({
        title: "Motos",
      }),
    },
    EditarVehiculo: {
      screen: EditarVehiculoScreen,
      navigationOptions: () => ({
        title: "Editar Vehiculo",
      }),
    },
    AgregarVehiculo: {
      screen: AgregarVehiculoScreen,
      navigationOptions: () => ({
        title: "Agregar Vehiculo",
      }),
    },
    Documentos: {
      screen: DocumentosScreen,
      navigationOptions: () => ({
        title: "Documentos de mi moto",
      }),
    },
    Servicios: {
      screen: ServiciosScreen,
      navigationOptions: () => ({
        title: "Servicios",
      }),
    },
    AgregarServicio: {
      screen: AgregarServicioScreen,
      navigationOptions: () => ({
        title: "Agregar servicio",
      }),
    },
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: "#041c24",
      },
      headerTintColor: "#0396c8",
      headerTitleAlign: "center",
      cardStyle: {
        backgroundColor: "#d7f8f8",
      },
      headerTitleStyle: {
        fontFamily: "IBMPlexSans-Bold",
      },
    },
  }
);

export default MotosScreenStacks;
