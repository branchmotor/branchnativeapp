import React from "react";
import { ActivityIndicator, StyleSheet, View, Text } from "react-native";
import { Overlay } from "react-native-elements";

export default function Loading(props: any) {
  const { isVisible, text } = props;
  return (
    <Overlay
      isVisible={true}
      windowBackgroundColor="rgba(4, 28, 36, .7)"
      overlayBackgroundColor="transparent"
      overlayStyle={style.overlay}
    >
      <View style={style.view}>
        <ActivityIndicator size="large" color="#5be5e5"></ActivityIndicator>
        {/* {text && <Text style={style.text}>{text}</Text>} */}
      </View>
    </Overlay>
  );
}

const style = StyleSheet.create({
  overlay: {
    height: "100%",
    width: "100%",
    // backgroundColor: "#fff",
    // borderColor: "#00a680",
    // borderWidth: 2,
    // borderRadius: 10,
    shadowOpacity: 0,
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: "#00a680",
    textTransform: "uppercase",
    marginTop: 10,
  },
});
