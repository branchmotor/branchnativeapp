import PropTypes from "prop-types";
import React, { useState, useEffect } from "react";
import { View, Platform, StyleSheet, KeyboardAvoidingView } from "react-native";
import { Image, Icon } from "react-native-elements";
import {
  GiftedChat,
  Send,
  InputToolbar,
  Bubble,
  Time,
} from "react-native-gifted-chat";
import CustomActions from "./CustomActions";
import KeyboardSpacer from "react-native-keyboard-spacer";
import auth from "@react-native-firebase/auth";
import { URL_SERVICES } from "react-native-dotenv";
import SocketIOClient from "socket.io-client";
import { v4 as uuidv4 } from "uuid";
import styles from "../../styles/App.scss";

const socket = SocketIOClient(URL_SERVICES, {
  transports: ["websocket"],
});

export default function Chat(props: any) {
  const { navigation } = props;
  const { IdTaller } = navigation.state.params;
  const [messages, setMessages] = useState([]);

  const user = () => {
    return {
      name: auth().currentUser?.displayName,
      _id: auth().currentUser?.uid,
    };
  };

  useEffect(() => {
    socket.on("connect", () => {
      console.log("Connection");
    });

    socket.emit(
      "joinroom",
      { room: auth().currentUser?.uid, IdTaller: IdTaller },
      (resultado: any) => {
        console.log("Resultado de unirse al room de firebase", resultado);
      }
    );

    fetch(
      URL_SERVICES +
        `message/getMessagesByConversacion?IdConversacionUser=${
          auth().currentUser?.uid
        }&IdTaller=${IdTaller}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        setMessages(GiftedChat.append(messages, json));
      })
      .catch((error) => console.error(error))
      .finally(() => {});

    return () => {
      console.log("Disconnect ::>");
    };
  }, []);

  useEffect(() => {
    socket.on("sendmessage", (message: any) => {
      setMessages(GiftedChat.append(messages, message));
    });
  }, [messages]);

  const onSend = (newmessage: never[]) => {
    setMessages(GiftedChat.append(messages, newmessage));
    socket.emit(
      "messaggetosomeone",
      auth().currentUser?.uid,
      parseMessage(newmessage[0])
    );
  };

  const parseMessage = (newmessage: any) => {
    let message = {
      _id: newmessage._id,
      createdAt: newmessage.createdAt,
      text: newmessage.text,
      delivered: false,
      read: false,
      user: {
        _id: newmessage.user._id,
        name: newmessage.user.name,
      },
      image: newmessage.image,
      IdConversacionUser: newmessage.user._id,
      IdTaller: IdTaller,
    };
    return message;
  };

  const onSendFromUser = (messages: any) => {
    const createdAt = new Date();
    const userMessage = user();
    const messagesToUpload = messages.map((message: any) => ({
      ...message,
      user: userMessage,
      createdAt,
      _id: uuidv4(),
    }));
    onSend(messagesToUpload);
  };

  return (
    <View style={styles.scrollContainer}>
      <GiftedChat
        messages={messages}
        onSend={(newMessage) => onSend(newMessage)}
        user={user()}
        placeholder="Escriba un mensaje"
        alwaysShowSend={true}
        renderUsernameOnMessage={true}
        renderBubble={renderBuble}
        scrollToBottom={true}
        scrollToBottomStyle={styles.chatScrollTo}
        renderTime={renderTime}
        renderInputToolbar={renderInputToolbar}
        textInputProps={styles.chatInput}
        renderActions={(props) => {
          return Platform.OS === "web" ? null : (
            <CustomActions {...props} onSend={onSendFromUser} />
          );
        }}
        renderSend={(props) => {
          return (
            <Send {...props} containerStyle={styles.chatSendContainer}>
              <View style={styles.iconButton}>
                <Image
                  source={require("./../../../assets/drawable-xxxhdpi/button.png")}
                  style={styles.iconButtonImage}
                />
                <Icon
                  containerStyle={styles.iconButtonIcon}
                  name="send"
                  type="material-community"
                  color="#0396c8"
                  size={25}
                />
              </View>
            </Send>
          );
        }}
      />
    </View>
  );
}

function renderInputToolbar(props: any) {
  return (
    <InputToolbar
      {...props}
      containerStyle={styles.chatToolbarContainer}
      placeholderTextColor={"#0396c8"}
    />
  );
}

function renderBuble(props: any) {
  return (
    <Bubble
      {...props}
      // renderTime={() => <Text>Time</Text>}
      // renderTicks={() => <Text>Ticks</Text>}
      wrapperStyle={{
        right: {
          backgroundColor: "#8bd2e8",
          borderRadius: 10,
          padding: 3,
          margin: 3,
        },
        left: {
          backgroundColor: "#a5e1ef",
          borderRadius: 10,
          padding: 3,
          margin: 3,
        },
      }}
      textStyle={{
        right: {
          color: "#041c24",
          fontSize: 15,
          fontFamily: "IBMPlexSans-Light",
        },
        left: {
          color: "#041c24",
          fontSize: 15,
          fontFamily: "IBMPlexSans-Light",
        },
      }}
    />
  );
}

function renderTime(props: any) {
  const { containerStyle, wrapperStyle, ...timeProps } = props;
  return (
    <Time
      {...timeProps}
      timeTextStyle={{
        right: {
          color: "#0396c8",
          fontFamily: "IBMPlexSans-Light",
        },
        left: {
          color: "#0396c8",
          fontFamily: "IBMPlexSans-Light",
        },
      }}
    />
  );
}
