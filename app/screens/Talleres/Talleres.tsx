import React, { useState, useEffect } from "react";
import styles from "../../styles/App.scss";
import auth from "@react-native-firebase/auth";
import { SafeAreaView } from "react-native";
import Loading from "../../components/Loading";
import ListTalleres from "../../components/talleres/ListTalleres";
import { URL_SERVICES } from "react-native-dotenv";
import OpenMap from "react-native-open-maps";

export default function Talleres(props: any) {
  const { navigation } = props;
  const [isLoading, setLoading] = useState(true);
  const [vehiculos, setLisVehiculo] = useState(new Array());

  console.log("Url services ::>", URL_SERVICES);

  const user = auth().currentUser;

  useEffect(() => {
    fetch(URL_SERVICES + "vehiculo/getByIdUsuario/" + user.uid)
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        setLisVehiculo(json);
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  const openAppMap = () => {
    OpenMap({
      zoom: 19,
      query: "Talleres"
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      {isLoading ? (
        <Loading isVisible={true} text="Cargando" />
      ) : (
        <ListTalleres
          vehiculos={vehiculos}
          navigation={navigation}
        ></ListTalleres>
      )}
    </SafeAreaView>
  );
}
