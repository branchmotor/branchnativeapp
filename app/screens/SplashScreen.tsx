import React, { useEffect } from "react";
import { SafeAreaView, ImageBackground, StyleSheet } from "react-native";

const FIVE_SECONDS = 5000;

export default function SplashScreen(props: any) {
  const { navigation } = props;

  useEffect(() => {
    setTimeout(() => {
      // Components that are placed inside a React Navigation
      // navigator will receive the `navigation` prop.
      // It's main usage is to trigger navigation events.
      // Right here we're telling it to navigate to the route
      // with the name 'App'.
      navigation.navigate("AuthLoading");
    }, FIVE_SECONDS);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground
        source={require("../../assets/bg_sin_logo.jpg")}
        style={styles.image}
      ></ImageBackground>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    flexDirection: "column",
    justifyContent: "center",
  },
});
