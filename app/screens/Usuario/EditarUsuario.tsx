import React, { useEffect } from "react";
import { View, Image, SafeAreaView } from "react-native";
import { Input, Button, Icon, Text } from "react-native-elements";
import { useForm } from "react-hook-form";
import Snackbar from "react-native-snackbar";
import { URL_SERVICES } from "react-native-dotenv";
import styles from "../../styles/App.scss";
import { ScrollView } from "react-native-gesture-handler";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default function EditarUsuario(props: any) {
  const { navigation } = props;
  const { usuario, usuarioFacebook, setReloadData } = navigation.state.params;
  const { register, handleSubmit, setValue, errors } = useForm();

  console.log("usuario facebook ::>", usuarioFacebook);

  useEffect(() => {
    register(
      { name: "nombre" },
      {
        required: { value: true, message: "Campo requerido" },
      }
    );
    register(
      { name: "identificacion" },
      {
        required: { value: true, message: "Campo requerido" },
      }
    );
    register(
      { name: "celular" },
      {
        required: { value: true, message: "Campo requerido" },
        pattern: {
          value: /^\+?[1-9]\d{9,14}$/,
          message: "Debe ingresar un numero de celular valido",
        },
      }
    );

    setValue(
      "nombre",
      usuarioFacebook ? usuarioFacebook.displayName : usuario.firstName + "",
      true
    );
    setValue("identificacion", usuario.identificacion + "", true);
    setValue("celular", usuario.celular + "", true);
  }, [register]);

  const updateUsuario = async (data: any) => {
    const usuarioUpdate = {
      email: usuario.email,
      identificacion: data.identificacion,
      celular: data.celular,
      firstName: data.nombre,
    };

    fetch(URL_SERVICES + "usuario/update/" + usuario.uid, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(usuarioUpdate),
    })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        console.log(json);
        if (json.error) {
          Snackbar.show({
            text: json.error,
            duration: Snackbar.LENGTH_SHORT,
          });
        } else {
          navigation.navigate("Usuario");
          Snackbar.show({
            text: "Se actualizo el usuario correctamente",
            duration: Snackbar.LENGTH_SHORT,
          });
        }
      })
      .catch((error) => console.error(error))
      .finally(() => {
        setReloadData(true);
      });
  };

  return (
    <SafeAreaView style={styles.scrollContainer}>
      <KeyboardAwareScrollView
        style={[styles.container, styles.userEditContainer]}
        extraScrollHeight={150}
        enableOnAndroid={true} 
        keyboardShouldPersistTaps='handled'
      >
        <View>
          <Text style={[styles.subheadingSecondary, styles.centerText, styles.regularMargin]}>Ahora puedes centralizar</Text>
          <Text style={[styles.headingSecondary, styles.centerText]}>toda la información de tus motos</Text>
        </View>
        <View style={[styles.containerAddPhoto, styles.regularMargin]}>
          <Image
            style={styles.userEditImage}
            source={require('./../../../assets/drawable-xxxhdpi/userEdit.png')}
          />
        </View>
        <Input
          labelStyle={styles.label}
          inputStyle={styles.input}
          label="Email"
          placeholder="Email"
          editable={false}
          defaultValue={usuario.email}
        />
        <Input
          labelStyle={styles.label}
          inputStyle={styles.input}
          label="Nombre Completo"
          placeholder="Nombre Completo"
          defaultValue={
            usuarioFacebook ? usuarioFacebook.displayName : usuario.firstName + ""
          }
          onChangeText={(text) => setValue("nombre", text, true)}
        />
        {errors.nombre && (
          <Text style={styles.inputError}>{errors.nombre.message}</Text>
        )}
        <Input
          labelStyle={styles.label}
          inputStyle={styles.input}
          label="Documento"
          placeholder="Documento"
          defaultValue={usuario.identificacion}
          onChangeText={(text) => setValue("identificacion", text, true)}
        />
        {errors.identificacion && (
          <Text style={styles.inputError}>{errors.identificacion.message}</Text>
        )}
        <Input
          labelStyle={styles.label}
          inputStyle={styles.input}
          label="Celular"
          placeholder="Celular"
          defaultValue={usuario.celular}
          onChangeText={(text) => setValue("celular", text, true)}
        />
        {errors.celular && (
          <Text style={styles.inputError}>{errors.celular.message}</Text>
        )}
        <Button
          title="Editar"
          onPress={handleSubmit(updateUsuario)}
          buttonStyle={styles.buttonPrimary}
          titleStyle={styles.buttonText}
        />
        <Button
          title="Cancelar"
          onPress={() => {
            navigation.navigate("Usuario");
          }}
          buttonStyle={[styles.buttonSecondary, {marginBottom:120}]}
          titleStyle={styles.buttonText}
        />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
}
