import React, { useState, useEffect } from "react";
import styles from "../../styles/App.scss";
import { View, SafeAreaView, Text, Platform, ScrollView } from "react-native";
import { Input, Button, Image } from "react-native-elements";
import auth from "@react-native-firebase/auth";
import { Dropdown } from "react-native-material-dropdown";
import DateTimePicker from "@react-native-community/datetimepicker";
import Snackbar from "react-native-snackbar";
import { URL_SERVICES } from "react-native-dotenv";
import { useForm } from "react-hook-form";
import Moment from "moment";
import { services } from "../../../data/data";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default function AgregarCita(props: any) {
  const { navigation } = props;
  const { setIsReloadData } = navigation.state.params;
  const [vehiculos, setVehiculos] = useState(new Array());
  const [isLoading, setLoading] = useState(true);
  const { register, handleSubmit, setValue, errors } = useForm();
  const [showCalendar, setShowCalendar] = useState(false);
  const [showTime, setShowTimeCalendar] = useState(false);
  const [fechaCita, setFechaCita] = useState(new Date());
  const [horaCita, setHoraCita] = useState(new Date());
  const [vehiculoSelect, setVehiculoSelect] = useState(null);

  const user = auth().currentUser;

  useEffect(() => {
    fetch(URL_SERVICES + "vehiculo/getByIdUsuario/" + user?.uid)
      .then((response) => response.json())
      .then((json) => {
        //console.log("Respuesta motos ::>", json);
        setVehiculos(json);
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    register(
      { name: "placa" },
      {
        required: { value: true, message: "Campo requerido" },
      }
    );
    register(
      { name: "fechacita" },
      {
        required: { value: true, message: "Campo requerido" },
      }
    );
    register(
      { name: "horacita" },
      {
        required: { value: true, message: "Campo requerido" },
      }
    );
    register(
      { name: "servicio" },
      {
        required: { value: true, message: "Campo requerido" },
      }
    );
  }, [register]);

  const createCita = async (data: any) => {
    let vehiculo = vehiculos.find((vehiculo) => {
      if (vehiculo.placa == data.placa) {
        return vehiculo;
      }
    });
    const citaCreate = {
      placa: data.placa,
      taller: vehiculo.taller.IdTaller, //TODO: Taller del vehiculo
      fechaCita: Moment(data.fechacita).format("DD/MM/YYYY"),
      horaCita: Moment(data.horacita).format("HH:mm"),
      servicio: data.servicio,
      estado: "Solicitada",
    };

    console.log("Data cita :::>", citaCreate);

    fetch(URL_SERVICES + "cita/create", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(citaCreate),
    })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        console.log("Respuesta de crear la cita ::>", json);
      })
      .catch((error) => console.error(error))
      .finally(() => {
        setIsReloadData(true);
        navigation.navigate("Citas");
      });
  };

  const selectVehiculo = (placa: String) => {
    let vehiculo = vehiculos.find((vehiculo) => {
      if (vehiculo.placa == placa) {
        return vehiculo;
      }
    });
    setVehiculoSelect(vehiculo);
  };

  return (
    <SafeAreaView>
      <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContainer}
        extraScrollHeight={150}
        enableOnAndroid={true} 
        keyboardShouldPersistTaps='handled'
      >
        <Text style={[styles.headingSecondary, styles.centerText]}>
          Estamos felices de poder ayudarte
        </Text>
        <View style={[styles.containerAddDatePhoto, styles.regularMargin]}>
          <Image
            style={styles.addDatePhoto}
            source={require("./../../../assets/drawable-xxxhdpi/addDate.png")}
          />
        </View>
        <Text
          style={[
            styles.subheadingSecondary,
            styles.centerText,
            styles.regularMargin,
          ]}
        >
          Agenda tu cita llenando estos datos
        </Text>
        <Dropdown
          label="Placa"
          containerStyle={[styles.input, styles.dropdown]}
          labelExtractor={(label: any) => {
            return label.placa;
          }}
          valueExtractor={(value: any) => {
            return value.placa;
          }}
          data={vehiculos}
          onChangeText={(text: any) => {
            selectVehiculo(text);
            setValue("placa", text, true);
          }}
        />
        {errors.placa && (
          <Text style={styles.inputError}>{errors.placa.message}</Text>
        )}
        {vehiculoSelect && !vehiculoSelect.taller && (
          <Text
            style={[
              styles.subheadingSecondary,
              styles.centerText,
              styles.regularMargin,
            ]}
          >
            No se puede agendar citas a esta vehiculo
          </Text>
        )}

        <Input
          labelStyle={styles.label}
          inputStyle={styles.input}
          label="Fecha"
          placeholder="Fecha"
          keyboardType="ascii-capable"
          onFocus={() => {
            setShowCalendar(true);
          }}
          value={Moment(fechaCita).format("DD/MM/YYYY")}
        />
        {showCalendar && (
          <DateTimePicker
            testID="datePicker"
            timeZoneOffsetInMinutes={0}
            value={new Date()}
            mode="date"
            is24Hour={true}
            display="default"
            locale="es-ES"
            onChange={(event, selectedDate) => {
              setShowCalendar(Platform.OS === "ios" ? true : false);
              setValue("fechacita", selectedDate, true);
              setFechaCita(selectedDate!);
            }}
            minimumDate={new Date()}
          />
        )}
        {errors.fechacita && (
          <Text style={styles.inputError}>{errors.fechacita.message}</Text>
        )}
        <Input
          labelStyle={styles.label}
          inputStyle={styles.input}
          label="Hora"
          placeholder="Hora"
          keyboardType="ascii-capable"
          onFocus={() => {
            setShowTimeCalendar(true);
          }}
          value={Moment(horaCita).format("hh:mm a")}
        />
        {showTime && (
          <DateTimePicker
            testID="TimePicker"
            value={new Date()}
            mode="time"
            is24Hour={false}
            display="default"
            locale="es-ES"
            onChange={(event, selectedDate) => {
              setShowTimeCalendar(Platform.OS === "ios" ? true : false);
              setValue("horacita", selectedDate, true);
              setHoraCita(selectedDate!);
            }}
          />
        )}
        {errors.horacita && (
          <Text style={styles.inputError}>{errors.horacita.message}</Text>
        )}
        <Dropdown
          label="Servicio"
          data={services}
          containerStyle={[styles.input, styles.dropdown]}
          onChangeText={(text: any) => {
            setValue("servicio", text, true);
          }}
        />
        {errors.servicio && (
          <Text style={styles.inputError}>{errors.servicio.message}</Text>
        )}
        <Button
          title="Agendar cita"
          onPress={handleSubmit(createCita)}
          buttonStyle={styles.buttonPrimary}
          titleStyle={styles.buttonText}
          disabled={vehiculoSelect && !vehiculoSelect.taller ? true : false}
        />
        <Button
          title="Cancelar"
          onPress={() => {
            navigation.navigate("Citas");
            console.log(navigation.navigate("Citas"))
          }}
          buttonStyle={styles.buttonSecondary}
          titleStyle={styles.buttonText}
        />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
}
