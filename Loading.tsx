import React from "react";
import { View, Text } from "react-native";

export default function loading(props: any) {
  const { text } = props;
  return (
    <View>
      <Text>Cargando {text}</Text>
    </View>
  );
}
